<?php

namespace App\Models\Gate;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GoogleAuth extends Model
{
    use HasFactory;

    protected $table = 'gate.google_auth';
    protected $guarded = [];
}
