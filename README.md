# TailwindCSS

Install TailwindCSS
```bash
npm install -D tailwindcss postcss autoprefixer
npx tailwindcss init -p
```

Run in Development
```bash
npm run dev
```
