<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, ...$roles): Response
    {
        if (!Auth::check()) {
            // The user is not authenticated
            return redirect('/login');
        }

        $has_roles = [];
        $user = User::with('user_role.role')->where('id', Auth::user()->id)->first();
        foreach($user->user_role as $userRole){
            $has_roles = $userRole->role->guard_name;
            if(in_array($has_roles, $roles))
                return $next($request);
        }

        return back()->with('error', 'Forbidden.');
    }
}
